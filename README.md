#HLF development Skeleton

One Organization with a single peer and and orderer node

# Start Network

> ./network.sh up       

# Create Channel

> ./network.sh createChannel -c testchannel  

# Set ENV Vars

> export PATH=${PWD}/../bin:$PATH                                                                                                  
> export FABRIC_CFG_PATH=$PWD/../config/
> export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
> export CORE_PEER_TLS_ENABLED=true                                                                                              
> export CORE_PEER_LOCALMSPID="Org1MSP"
> export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
> export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
> export CORE_PEER_ADDRESS=localhost:7051

# Package Chaincode

> peer lifecycle chaincode package property.tar.gz --path ../chaincode/property-app/go/ --lang golang --label property_1  

# Install Chaincode

> peer lifecycle chaincode install property.tar.gz --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE

# Approve Chaincode

> peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --sequence 1 --cafile $ORDERER_CA --channelID testchannel --name property --version 1.0 --init-required --package-id property_1:3a25f73e56203ba141cf42beae306ebe790f48befcbc47937ba0de4927f43f18

# Commit Chaincode

> peer lifecycle chaincode commit -o  localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA --channelID testchannel --name property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE --version 1.0 --sequence 1 --init-required

# Sample Queries

> peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C testchannel -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE  --isInit -c '{"Args":[]}' 

> peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C testchannel -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE -c '{"Args":["AddProperty", "1", "113 Main St", "1000", "John","115000"]}' 

> peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C testchannel -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE -c '{"Args":["AddProperty", "2", "286 Meadow Street", "6000", "Paul Jones","250000"]}'

> peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C testchannel -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE -c '{"Args":["QueryAllProperties"]}'

